# first debug curl post test :
# curl -i -X POST -H 'Content-Type: application/json' -d '{"fan_id":951908,"older_than_token":"1534836284:3389753074:a::","count":40}' https://bandcamp.com/api/fancollection/1/wishlist_items

# importing the requests library
import requests
import json
from bs4 import BeautifulSoup
import argparse
from urllib import parse

# Récupérons l'argument de l'utilisateur, qui est tout simplement le user bc
parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument("currentUser", help="Entrez votre user bandcamp ! Merci jpp.")
args = vars(parser.parse_args()) # Digger ce truc car c'est paul le PGM qui m'a montré lel
currentUser = args["currentUser"]
print("On va chercher les concerts de ton user : " + currentUser)

print("Voici le lien de votre wishlist dans laquelle nous allons chercher votre fan_id bandcamp!")
page_link = "https://bandcamp.com/" + currentUser + "/wishlist"
print(page_link)

# query the website and return the html to the variable ‘page’
page_response = requests.get(page_link, timeout=5)

# parse the html using beautiful soup and store in variable "soup"
soup = BeautifulSoup(page_response.content, "html.parser")

# We need to find a gift link to get fan_id, which is in a <li class="send-as-gift"
# then inside a href :
giftClass = soup.find_all(class_="send-as-gift")[0]

giftLink = giftClass.a.get('href')

# Exemple d'url avec un fan_id, saut à la ligne à partir du query:
# https://oraculorecords.bandcamp.com/album/scratch-massive-golden-dreams-nuit-de-mes-r-ves-12-ltd-edition?
# action=gift
# &
# fan_id=951908
# &
# recipient=rozlav

fanId = dict(parse.parse_qsl(parse.urlsplit(giftLink).query))["fan_id"]
print("Votre fan_id bandcamp est : " + fanId)

# defining the api-endpoint
api_url = "https://bandcamp.com/api/fancollection/1/wishlist_items"

# some random API key here yey
randomOneHourTokenLel = "1534836284:3389753074:a::"

# data to be sent to api
# example : {"fan_id":951908,"older_than_token":"1534836284:3389753074:a::","count":40}
bcData = {'fan_id':int(fanId),
        'older_than_token':randomOneHourTokenLel,
        'count':666}

# sending post request and saving response as response object
headers = {'content-type': 'application/json'}
response = requests.post(api_url, data=json.dumps(bcData), headers=headers)

# converting response to json
responseJSON = response.json()
allBands = []

for i in responseJSON['items']:
    currentBand = i['band_url']
    allBands.append(currentBand)

allBandsUnique = list(set(allBands))

nbBands = len(allBandsUnique)

print("Nombre de groupes : %s" % nbBands)

print("----------------------------------------")
for currentPageLink in allBandsUnique:
    print("entering " + currentPageLink)
    if currentPageLink.startswith("http://bandcamp.com/"):
        print("that is user so nope")
    else:
        currentResponse = requests.get(currentPageLink, timeout=5)

        currentSoup = BeautifulSoup(currentResponse.content, "html.parser")

        laDate = currentSoup.find("div", {"class": "showDate"})
        if laDate is None:
            print("Pas d'event actuellement...")
        else:
            print("/!\ ÉVÈNEMENT /!\ ")
            arti = currentSoup.find("span", {"class": "title"})
            artiContent = arti.text.strip()
            salle = currentSoup.find("div", {"class": "showVenue"})
            ville = currentSoup.find("div", {"class": "showLoc"})
            laDateCon = laDate.text.strip()
            salleCon = salle.text.strip()
            villeCon = ville.text.strip()
            print(artiContent)
            print(laDateCon)
            print(salleCon)

            # Extracting URL from the attribute href in the <a> tags from lieuCon.
            for hrefSalle in salle:
                print(hrefSalle.get("href"))
            print(villeCon)
            print("/!\ ÉVÈNEMENT /!\ ")

    print("----------------------------------------")
print("end.")
